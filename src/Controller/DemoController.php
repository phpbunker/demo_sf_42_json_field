<?php

namespace App\Controller;

use App\ClassDump\Lambdata;
use App\Entity\Demo;
use App\Repository\DemoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class DemoController extends AbstractController
{
    /**
     * @Route("/", name="demo_array")
     */
    public function manipulateJson(): Response
    {
        $demo = new Demo();
        $demo->setAttribute([
            'label' => 'Le label',
            'value' => 'La value',
        ]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($demo);
        $entityManager->flush();

        return new Response(var_export($demo), Response::HTTP_OK, ["content-type" => "text/plain"]);
    }

    /**
     * @Route("/obj", name="demo_mano")
     */
    public function manipulateJsonObject(DemoRepository $repository): Response
    {
        $demo = new Demo();
        $demo->setAttribute([
            new Lambdata('My label', 'My Value'),
            new Lambdata('Your label', 'Your Value'),
        ]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($demo);
        $entityManager->flush();
        $id = $demo->getId();
        unset($demo);
        $demo = $repository->find($id);

        return new Response(var_export($demo), Response::HTTP_OK, ["content-type" => "text/plain"]);
    }
}
