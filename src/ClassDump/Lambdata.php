<?php


namespace App\ClassDump;


class Lambdata implements \JsonSerializable
{
    private $label;
    private $value;

    /**
     * Lambdata constructor.
     * @param $label
     * @param $value
     */
    public function __construct($label, $value)
    {
        $this->label = $label;
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    public function jsonSerialize ()
    {
        return [
            'label' => $this->label,
            'value' => $this->value,
        ];
    }

}