<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DemoRepository")
 */
class Demo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $attribute = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAttribute(): ?array
    {
        return $this->attribute;
    }

    public function setAttribute(array $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }
}
